package com.alivanov.lws.model;

import com.alivanov.lws.expr.DoubleValue;
import com.alivanov.lws.expr.StringValue;
import org.junit.Test;

import static org.junit.Assert.*;

public class GridValueCellReadAndWriteTest {

    @Test
    public void testRemoveEmptyCell() {
        CellGrid grid = new CellGrid();
        grid.remove(Pos.from("A1"));

        assertEquals(null, grid.get(Pos.from("A1")));
    }
    @Test
    public void testAddStringValueCell() {
        CellGrid grid = new CellGrid();
        Cell cell = GridUtils.addValueCell(grid, Pos.from("A1"), new StringValue("abc"));

        assertEquals(cell, grid.get(Pos.from("A1")));
        assertEquals(new StringValue("abc"), grid.get(Pos.from("A1")).getValue());
        assertEquals(new StringValue("abc").toString(), grid.get(Pos.from("A1")).getValue().toString());
    }

    @Test
    public void testAddNumberValueCell() {
        CellGrid grid = new CellGrid();
        Cell cell = GridUtils.addValueCell(grid, Pos.from("A1"), new DoubleValue(1));

        assertEquals(cell, grid.get(Pos.from("A1")));
        assertEquals(new DoubleValue(1), grid.get(Pos.from("A1")).getValue());
    }

    @Test
    public void testReplaceValueCell() {
        CellGrid grid = new CellGrid();
        Cell first = GridUtils.addValueCell(grid, Pos.from("A1"), new DoubleValue(1));
        Cell second = GridUtils.addValueCell(grid, Pos.from("A1"), new DoubleValue(2));

        Cell actual = grid.get(Pos.from("A1"));
        assertNotEquals(first, actual);
        assertEquals(second, actual);
        assertEquals(new DoubleValue(2), grid.get(Pos.from("A1")).getValue());
    }

    @Test
    public void testRemoveValueCell() {
        CellGrid grid = new CellGrid();
        Cell first = GridUtils.addValueCell(grid, Pos.from("A1"), new DoubleValue(1));
        Cell second = GridUtils.addValueCell(grid, Pos.from("A2"), new DoubleValue(2));

        grid.remove(Pos.from("A2"));

        Cell actual = grid.get(Pos.from("A1"));
        assertEquals(first, actual);
        assertNotEquals(second, actual);
        assertNull(grid.get(Pos.from("A2")));
    }
}
