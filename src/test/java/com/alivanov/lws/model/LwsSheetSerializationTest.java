package com.alivanov.lws.model;

import com.alivanov.lws.expr.DoubleValue;
import com.alivanov.lws.expr.StringValue;
import com.alivanov.lws.ui.LwsApplicationProperties;
import org.junit.Test;

import java.io.IOException;

import static com.alivanov.lws.Utils.restore;
import static com.alivanov.lws.Utils.serialize;
import static org.junit.Assert.assertEquals;

public class LwsSheetSerializationTest {

    @Test
    public void testSaveAndRestore() throws IOException, ClassNotFoundException {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from(0, 0), new DoubleValue(1))
                .withValueCell(Pos.from(4, 4), new StringValue("abc"))
                .withFormulaCell(Pos.from(1, 1), "A1 + 2")
                .withFormulaCell(Pos.from(2, 2), "A1 + B2")
                .withFormulaCell(Pos.from(3, 2), "D8")
                .build();

        LwsSheet sheet = new LwsSheet(LwsApplicationProperties.DEFAULT_ROW_COUNT, LwsApplicationProperties.DEFAULT_COLUMN_COUNT,
                grid);

        byte[] bytes = serialize(sheet);
        LwsSheet restoredSheet = (LwsSheet) restore(bytes);

        assertEquals("Sheet is corrupted after serialization", sheet, restoredSheet);
        assertEquals("String representation is not equal", sheet.toString(), restoredSheet.toString());
    }

}
