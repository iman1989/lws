package com.alivanov.lws.model;

import com.alivanov.lws.expr.Value;

public class GridBuilder {
    private CellGrid grid;
    private boolean immediateCalculation;

    public static GridBuilder create() {
        GridBuilder builder = new GridBuilder();

        builder.grid = new CellGrid();
        builder.immediateCalculation = true;

        return builder;
    }

    public GridBuilder withValueCell(Pos pos, Value value) {
        if (immediateCalculation) {
            GridUtils.addValueCell(this.grid, pos, value);
        } else {
            GridUtils.addValueCellWithoutRecalculation(this.grid, pos, value);
        }

        return this;
    }

    public GridBuilder withFormulaCell(Pos pos, String formula) {
        if (immediateCalculation) {
            GridUtils.addFormulaCell(this.grid, pos, formula);
        } else {
            GridUtils.addFormulaCellWithoutRecalculation(this.grid, pos, formula);
        }

        return this;
    }

    public CellGrid build() {
        return grid;
    }
}
