package com.alivanov.lws.model;

import com.alivanov.lws.expr.DoubleValue;
import com.alivanov.lws.expr.Formula;
import com.alivanov.lws.expr.StringValue;
import org.junit.Before;
import org.junit.Test;

import static com.alivanov.lws.ui.LwsApplicationProperties.DEFAULT_COLUMN_COUNT;
import static com.alivanov.lws.ui.LwsApplicationProperties.DEFAULT_ROW_COUNT;
import static org.junit.Assert.*;

public class LwsTableModelTest {

    private LwsTableModel lwsTableModel;

    @Before
    public void init() {
        lwsTableModel = new LwsTableModel(DEFAULT_ROW_COUNT, DEFAULT_COLUMN_COUNT);
    }

    @Test
    public void testCreateLwsModel() {
        assertEquals(DEFAULT_ROW_COUNT, lwsTableModel.getRowCount());
        assertEquals(DEFAULT_COLUMN_COUNT, lwsTableModel.getRowCount());

        lwsTableModel = new LwsTableModel(100, 50);
        assertEquals(100, lwsTableModel.getRowCount());
        assertEquals(50, lwsTableModel.getColumnCount());
    }

    @Test
    public void testSaveAndLoadFromLwsSheet() {
        lwsTableModel = new LwsTableModel(100, 50);

        CellGrid grid = lwsTableModel.getGrid();
        GridUtils.addValueCell(grid, Pos.from(0, 0), new DoubleValue(1));
        GridUtils.addValueCell(grid, Pos.from(0, 1), new DoubleValue(1));
        GridUtils.addFormulaCell(grid, Pos.from(1, 1), "A1 + B1");

        LwsSheet lwsSheet = lwsTableModel.saveToSheet();

        LwsTableModel previous = lwsTableModel;
        lwsTableModel.loadFromSheet(lwsSheet);

        assertEquals(previous.getRowCount(), lwsTableModel.getRowCount());
        assertEquals(previous.getColumnCount(), lwsTableModel.getColumnCount());
        assertEquals(previous.getGrid(), lwsTableModel.getGrid());
    }

    @Test
    public void testGetValueCellAt() {
        CellGrid grid = lwsTableModel.getGrid();
        Cell expected = GridUtils.addValueCell(grid, Pos.from(0, 0), new DoubleValue(1));

        Cell actual = lwsTableModel.getValueAt(0, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void testGetFormulaCellAt() {
        CellGrid grid = lwsTableModel.getGrid();
        Cell expected = GridUtils.addFormulaCell(grid, Pos.from(0, 0), "1/3 + 2/3");

        Cell actual = lwsTableModel.getValueAt(0, 0);
        assertEquals(expected, actual);
    }

    @Test
    public void testSetNullValueInEmptyGrid() {
        lwsTableModel.setValueAt(null,0, 0);
        assertNull(lwsTableModel.getValueAt(0, 0));
        assertEquals(0, lwsTableModel.getGrid().size());
    }

    @Test
    public void testSetValueNotStringClass() {
        lwsTableModel.setValueAt(new Object(),0, 0);
        assertNull(lwsTableModel.getValueAt(0, 0));
        assertEquals(0, lwsTableModel.getGrid().size());
    }

    @Test
    public void testSetStringValue() {
        testString("abc", 0, 0);
    }

    @Test
    public void testSetDoubleValueFromIntLiteral() {
        String str = "123";
        lwsTableModel.setValueAt(str, 0, 0);

        Cell cell = lwsTableModel.getValueAt(0, 0);
        assertTrue(cell.getValue() instanceof DoubleValue);
        assertTrue(new Double(123).equals(cell.getValue().toDoubleValue()));
        assertEquals(str, cell.getValue().toString());
    }

    @Test
    public void testSetDoubleValueFromFloatLiteral() {
        String str = "123.1";
        lwsTableModel.setValueAt(str, 0, 0);

        Cell cell = lwsTableModel.getValueAt(0, 0);
        assertTrue(cell.getValue() instanceof DoubleValue);
        assertTrue(new Double(123.1).equals(cell.getValue().toDoubleValue()));
        assertEquals(str, cell.getValue().toString());
    }

    @Test
    public void testSetStringFromIncompleteFormula() {
        testString("=", 0, 0);
    }

    @Test
    public void testSetFromFormulaValueWithoutEqualsSign() {
        testString("A + B", 0, 0);
    }

    @Test
    public void testSetValidFormulaValue() {
        String str = "=A1 + B1";
        lwsTableModel.setValueAt(str, 0, 0);

        Cell cell = lwsTableModel.getValueAt(0, 0);
        assertTrue(cell instanceof FormulaCell);

        Formula formula = ((FormulaCell) cell).getFormula();
        assertEquals("A1 + B1", formula.getFormulaText());
        assertTrue(formula.isValid());
    }

    @Test
    public void testSetInvalidFormulaValue() {
        String str = "=A + 3B";
        lwsTableModel.setValueAt(str, 0, 0);

        Cell cell = lwsTableModel.getValueAt(0, 0);
        assertTrue(cell instanceof FormulaCell);

        Formula formula = ((FormulaCell) cell).getFormula();
        assertEquals("A + 3B", formula.getFormulaText());
        assertTrue(!formula.isValid());
    }

    @Test
    public void testRemoveValue() {
        lwsTableModel.setValueAt("1", 0, 0);
        lwsTableModel.setValueAt(null, 0, 0);

        assertNull(lwsTableModel.getValueAt(0, 0));
        assertEquals(0, lwsTableModel.getGrid().size());

        lwsTableModel.setValueAt("1", 0, 0);
        lwsTableModel.setValueAt("", 0, 0);

        assertNull(lwsTableModel.getValueAt(0, 0));
        assertEquals(0, lwsTableModel.getGrid().size());
    }

    @Test
    public void testRewriteValue() {
        lwsTableModel.setValueAt("1", 0, 0);
        Cell original = lwsTableModel.getValueAt(0, 0);

        lwsTableModel.setValueAt("cde", 0, 0);
        Cell rewritten = lwsTableModel.getValueAt(0, 0);

        assertTrue(original != rewritten);
        testString("cde", 0, 0);
    }

    @Test
    public void testNotRewriteSameValueCell() {
        testRewrite("1", 0, 0);
    }

    @Test
    public void testNotRewriteSameFormulaCell() {
        testRewrite("=A1 + B1", 0, 0);
    }

    private void testString(String expected, int row, int col) {
        lwsTableModel.setValueAt(expected, row, col);

        Cell cell = lwsTableModel.getValueAt(row, col);
        assertTrue(cell.getValue() instanceof StringValue);
        assertEquals(expected, cell.getValue().toString());
    }

    private void testRewrite(String str, int row, int col) {
        lwsTableModel.setValueAt(str, row, col);
        Cell original = lwsTableModel.getValueAt(row, col);

        lwsTableModel.setValueAt(str, row, col);
        Cell rewritten = lwsTableModel.getValueAt(row, col);

        // Test that cell is not replaced
        assertTrue(original == rewritten);
    }
}