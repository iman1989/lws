package com.alivanov.lws.model;

import com.alivanov.lws.expr.ComputationContext;
import com.alivanov.lws.expr.DoubleValue;
import org.junit.Test;

import java.util.StringJoiner;

import static org.junit.Assert.assertEquals;

public class GridVarFormulaCalculationTest {

    public static final String FORMULA_COMPUTATION_IS_NOT_CORRECT = "Formula computation is not correct";

    @Test
    public void testSimpleVariableCalculation() {
        CellGrid grid = new CellGrid();
        GridUtils.addValueCellWithoutRecalculation(grid, Pos.from("A1"), new DoubleValue(1));
        GridUtils.addValueCellWithoutRecalculation(grid, Pos.from("B1"), new DoubleValue(1));
        Cell cell = GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("C1"), "A1 + B1");
        cell.compute(ComputationContext.createNew(grid));
        assertEquals(FORMULA_COMPUTATION_IS_NOT_CORRECT, new DoubleValue(2), cell.getValue());
    }

    @Test
    public void testNestedVariableCalculationFirst() {
        CellGrid grid = new CellGrid();
        GridUtils.addValueCellWithoutRecalculation(grid, Pos.from("A1"), new DoubleValue(1));
        GridUtils.addValueCellWithoutRecalculation(grid, Pos.from("B1"), new DoubleValue(1));
        GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("C1"), "A1 + B1");
        Cell cell = GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("D1"), "C1 + 2");
        cell.compute(ComputationContext.createNew(grid));
        assertEquals(FORMULA_COMPUTATION_IS_NOT_CORRECT, new DoubleValue(4), cell.getValue());
    }

    @Test
    public void testNestedVariableCalculationSecond() {
        CellGrid grid = new CellGrid();
        GridUtils.addValueCellWithoutRecalculation(grid, Pos.from("A1"), new DoubleValue(1));
        GridUtils.addValueCellWithoutRecalculation(grid, Pos.from("B1"), new DoubleValue(1));
        GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("C1"), "A1 + B1");
        Cell cell = GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("D1"), "C1 + (A1 * B1)");
        cell.compute(ComputationContext.createNew(grid));
        assertEquals(FORMULA_COMPUTATION_IS_NOT_CORRECT, new DoubleValue(3), cell.getValue());
    }

    @Test
    public void testRepeatedVariableCalculation() {
        CellGrid grid = new CellGrid();
        GridUtils.addValueCellWithoutRecalculation(grid, Pos.from("A1"), new DoubleValue(1));
        Cell cell = GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("B1"), "A1 + A1");
        cell.compute(ComputationContext.createNew(grid));
        assertEquals(FORMULA_COMPUTATION_IS_NOT_CORRECT, new DoubleValue(2), cell.getValue());
    }

    /*
        This test will not be executed in time unless formula result caching is working properly
     */
    @Test
    public void testRepeatedFormulaCalculation() {
        CellGrid grid = new CellGrid();
        GridUtils.addValueCellWithoutRecalculation(grid, Pos.from("A1"), new DoubleValue(1));
        GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("B1"), createSumFormula("A1", 16));
        GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("C1"), createSumFormula("B1", 16));
        GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("D1"), createSumFormula("C1", 16));
        GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("E1"), createSumFormula("D1", 16));
        GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("F1"), createSumFormula("E1", 16));
        GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("G1"), createSumFormula("F1", 16));
        Cell cell = GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("H1"), createSumFormula("G1", 16));
        cell.compute(ComputationContext.createNew(grid));
        assertEquals(FORMULA_COMPUTATION_IS_NOT_CORRECT, new DoubleValue(Math.pow(16, 7)), cell.getValue());
    }

    private String createSumFormula(String sheetName, int times) {
        StringJoiner joiner = new StringJoiner(" + ");
        for (int i = 0; i < times; i++) {
            joiner.add(sheetName);
        }
        return joiner.toString();
    }
}