package com.alivanov.lws.model;

import com.alivanov.lws.expr.FormulaParser;
import com.alivanov.lws.expr.Value;

public class GridUtils {

    private GridUtils() {

    }

    public static ValueCell addValueCellWithoutRecalculation(CellGrid grid, Pos pos, Value value) {
        ValueCell valueCell = new ValueCell(pos, value);
        grid.getData().put(pos, valueCell);
        return valueCell;
    }

    public static ValueCell addValueCell(CellGrid grid, Pos pos, Value value) {
        ValueCell valueCell = new ValueCell(pos, value);
        grid.write(valueCell);
        return valueCell;
    }

    public static FormulaCell addFormulaCellWithoutRecalculation(CellGrid grid, Pos pos, String formula) {
        FormulaCell formulaCell = new FormulaCell(pos, new FormulaParser().parseFormula(formula));
        grid.getData().put(pos, formulaCell);
        grid.addDependencyToParents(formulaCell);
        return formulaCell;
    }

    public static FormulaCell addFormulaCell(CellGrid grid, Pos pos, String formula) {
        FormulaCell formulaCell = new FormulaCell(pos, new FormulaParser().parseFormula(formula));
        grid.write(formulaCell);
        grid.addDependencyToParents(formulaCell);
        return formulaCell;
    }
}
