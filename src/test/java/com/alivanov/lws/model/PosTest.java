package com.alivanov.lws.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

public class PosTest {

    public static final String CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT = "Converting sheet coordinates to actual is incorrect";
    public static final String CONVERTING_FROM_ACTUAL_COORDINATES_TO_SHEET_COORDINATES_IS_INCORRECT = "Converting from actual coordinates to sheet coordinates is incorrect";

    @Test
    public void testTranslationFromSheetCoordinates() {
        Pos actual = Pos.from(null);
        assertNull(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, actual);

        int row = 0;
        int col = 0;
        Pos expected = Pos.from(row, col);
        actual = Pos.from("A1");
        assertEquals(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, row, actual.getRow());
        assertEquals(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, row, actual.getCol());
        assertEquals(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, expected, actual);

        expected = Pos.from(0, 26);
        actual = Pos.from("AA1");
        assertEquals(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, expected, actual);

        expected = Pos.from(120, 0);
        actual = Pos.from("A121");
        assertEquals(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, expected, actual);

        expected = Pos.from(29, 27);
        actual = Pos.from("AB30");
        assertEquals(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, expected, actual);

        expected = Pos.from(0, 0);
        actual = Pos.from("a1");
        assertEquals(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, expected, actual);

        expected = Pos.from(0, 26);
        actual = Pos.from("Aa1");
        assertEquals(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, expected, actual);

        expected = Pos.from(0, 26);
        actual = Pos.from("aA1");
        assertEquals(CONVERTING_SHEET_COORDINATES_TO_ACTUAL_IS_INCORRECT, expected, actual);
    }

    @Test
    public void testTranslationToSheetCoordinates() {
        Pos pos = Pos.from(0, 0);
        String expected = "A1";
        assertEquals(CONVERTING_FROM_ACTUAL_COORDINATES_TO_SHEET_COORDINATES_IS_INCORRECT, expected, pos.toSheetCoordinates());

        pos = Pos.from(0, 26);
        expected = "AA1";
        assertEquals(CONVERTING_FROM_ACTUAL_COORDINATES_TO_SHEET_COORDINATES_IS_INCORRECT, expected, pos.toSheetCoordinates());

        pos = Pos.from(120, 0);
        expected = "A121";
        assertEquals(CONVERTING_FROM_ACTUAL_COORDINATES_TO_SHEET_COORDINATES_IS_INCORRECT, expected, pos.toSheetCoordinates());

        pos = Pos.from(29, 27);
        expected = "AB30";
        assertEquals(CONVERTING_FROM_ACTUAL_COORDINATES_TO_SHEET_COORDINATES_IS_INCORRECT, expected, pos.toSheetCoordinates());
    }

    @Test(expected = NumberFormatException.class)
    public void testInvalidCoordinatesTranslation() {
        // TODO add validation to pos
        Pos.from("112DF");
        fail();
    }

    @Test
    public void testPosToString() {
        String expected = "(1, 1)";
        String actual = Pos.from(1, 1).toString();
        assertEquals(expected, actual);
    }

}