package com.alivanov.lws.model;

import com.alivanov.lws.expr.DoubleValue;
import com.alivanov.lws.expr.ErrorValue;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;

import static com.alivanov.lws.model.GridUtils.addValueCell;
import static org.junit.Assert.*;

public class GridDependencyCalculationTest {

    public static final String CELL_COMPUTATION_WAS_INCORRECT = "Cell computation was incorrect";
    public static final String DEPENDENCIES_NOT_CHANGED_CORRECTLY = "Dependencies not changed correctly";

    @Test
    public void testAddSimpleFormulaCell() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withValueCell(Pos.from("A2"), new DoubleValue(2))
                .withFormulaCell(Pos.from("B2"), "A1 + A2")
                .build();
        Cell cell = grid.get(Pos.from("B2"));

        assertEquals(grid.get(Pos.from("B2")), cell);
        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(3), cell.getValue());
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(Arrays.asList(Pos.from("B2"))),
                grid.getDependencies(Pos.from("A1")));
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(Arrays.asList(Pos.from("B2"))),
                grid.getDependencies(Pos.from("A2")));
    }

    @Test
    public void testRemoveSimpleFormulaCell() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withValueCell(Pos.from("A2"), new DoubleValue(2))
                .withFormulaCell(Pos.from("B2"), "A1 + A2")
                .build();

        grid.remove(Pos.from("B2"));

        assertNull(grid.get(Pos.from("B2")));
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(),
                grid.getDependencies(Pos.from("A1")));
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(),
                grid.getDependencies(Pos.from("A2")));
    }

    @Test
    public void testModifyValueCellToFormulaCell() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withValueCell(Pos.from("A2"), new DoubleValue(2))
                .withValueCell(Pos.from("B2"), new DoubleValue(3))
                .withFormulaCell(Pos.from("B2"), "A1 + A2")
                .build();

        Cell cell = grid.get(Pos.from("B2"));
        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(3), cell.getValue());
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(Arrays.asList(Pos.from("B2"))),
                grid.getDependencies(Pos.from("A1")));
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(Arrays.asList(Pos.from("B2"))),
                grid.getDependencies(Pos.from("A2")));
    }

    @Test
    public void testModifyFormulaCellToValueCell() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withValueCell(Pos.from("A2"), new DoubleValue(2))
                .withFormulaCell(Pos.from("B2"), "A1 + A2")
                .withValueCell(Pos.from("B2"), new DoubleValue(3))
                .build();

        Cell cell = grid.get(Pos.from("B2"));
        assertEquals(grid.get(Pos.from("B2")), cell);
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(),
                grid.getDependencies(Pos.from("A1")));
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(),
                grid.getDependencies(Pos.from("A2")));
    }

    @Test
    public void testModifyFormulaCell() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withValueCell(Pos.from("A2"), new DoubleValue(2))
                .withFormulaCell(Pos.from("B2"), "A1 + A2")
                .withValueCell(Pos.from("A3"), new DoubleValue(3))
                .withFormulaCell(Pos.from("B2"), "A1 + A3")
                .build();
        Cell cell = grid.get(Pos.from("B2"));

        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(4), cell.getValue());
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(Arrays.asList(Pos.from("B2"))),
                grid.getDependencies(Pos.from("A1")));
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(),
                grid.getDependencies(Pos.from("A2")));
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(Arrays.asList(Pos.from("B2"))),
                grid.getDependencies(Pos.from("A3")));
    }

    @Test
    public void testRemoveDependantValueCell() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withValueCell(Pos.from("A2"), new DoubleValue(2))
                .withValueCell(Pos.from("B2"), new DoubleValue(3))
                .withFormulaCell(Pos.from("C3"), "A1 * B2")
                .build();
        Cell cell = grid.get(Pos.from("C3"));
        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(3), cell.getValue());

        grid.remove(Pos.from("B2"));

        assertNull(grid.get(Pos.from("B2")));
        assertTrue(cell.getValue() instanceof ErrorValue);
        assertEquals(ErrorValue.ErrorCode.MISSING_CELL.toString(), cell.getValue().toString());
        // Dependencies should still exists even if the cell is deleted
        assertEquals(DEPENDENCIES_NOT_CHANGED_CORRECTLY,
                new HashSet<>(Arrays.asList(Pos.from("C3"))),
                grid.getDependencies(Pos.from("B2")));
    }

    @Test
    public void testModifyValueCellWithOneDependency() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withValueCell(Pos.from("A2"), new DoubleValue(2))
                .withFormulaCell(Pos.from("B2"), "A1 + A2")
                .build();
        Cell cell = grid.get(Pos.from("B2"));

        addValueCell(grid, Pos.from("A1"), new DoubleValue(3));
        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(5), cell.getValue());
    }

    @Test
    public void testModifyValueCellWithSeveralDependencies() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withFormulaCell(Pos.from("B2"), "A1 + 2")
                .withFormulaCell(Pos.from("C3"), "A1 + B2")
                .build();

        Cell firstFormulaCell = grid.get(Pos.from("B2"));
        Cell secondFormulaCell = grid.get(Pos.from("C3"));

        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(3), firstFormulaCell.getValue());
        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(4), secondFormulaCell.getValue());

        addValueCell(grid, Pos.from("A1"), new DoubleValue(3));
        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(5), firstFormulaCell.getValue());
        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(8), secondFormulaCell.getValue());
    }

    @Test
    public void testModifyFormulaCellWithDependencies() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withValueCell(Pos.from("A2"), new DoubleValue(2))
                .withValueCell(Pos.from("A3"), new DoubleValue(3))
                .withFormulaCell(Pos.from("B1"), "A1 + A2")
                .withFormulaCell(Pos.from("B2"), "A2 + A3")
                .withFormulaCell(Pos.from("C1"), "B1 + B2")
                .build();

        Cell resultFormulaCell = grid.get(Pos.from("C1"));
        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(8), resultFormulaCell.getValue());

        GridUtils.addFormulaCell(grid, Pos.from("B1"), "A1 - A2");
        assertEquals(CELL_COMPUTATION_WAS_INCORRECT, new DoubleValue(4), resultFormulaCell.getValue());
    }

}