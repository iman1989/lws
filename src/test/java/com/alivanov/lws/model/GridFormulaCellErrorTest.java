package com.alivanov.lws.model;

import com.alivanov.lws.expr.DoubleValue;
import com.alivanov.lws.expr.ErrorValue;
import com.alivanov.lws.expr.StringValue;
import com.alivanov.lws.expr.Value;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GridFormulaCellErrorTest {

    @Test
    public void testDivisionByZeroError() {
        CellGrid grid = new CellGrid();
        GridUtils.addValueCell(grid, Pos.from("A1"), new DoubleValue(1));
        GridUtils.addValueCell(grid, Pos.from("B1"), new DoubleValue(0));

        GridUtils.addFormulaCell(grid, Pos.from("B2"), "A1 / B1");

        Value value = grid.get(Pos.from("B2")).getValue();

        assertTrue(value instanceof ErrorValue);
        assertEquals(ErrorValue.ErrorCode.COMPUTATION_ERROR.toString(), value.toString());
        assertEquals("Division by zero", ((ErrorValue) value).getCause());
    }

    @Test
    public void testEmptyCellReferenceError() {
        CellGrid grid = new CellGrid();
        GridUtils.addFormulaCell(grid, Pos.from("A1"), "B2");

        Value value = grid.get(Pos.from("A1")).getValue();

        assertTrue(value instanceof ErrorValue);
        assertEquals(ErrorValue.ErrorCode.MISSING_CELL.toString(), value.toString());
        assertEquals("No data found in cell B2", ((ErrorValue) value).getCause());
    }

    @Test
    public void testInvalidFormulaError() {
        CellGrid grid = new CellGrid();
        GridUtils.addFormulaCell(grid, Pos.from("A1"), "abc / def + 1");

        Value value = grid.get(Pos.from("A1")).getValue();

        assertTrue(value instanceof ErrorValue);
        assertEquals(ErrorValue.ErrorCode.INVALID_FORMULA.toString(), value.toString());
        assertEquals("Entered formula is invalid", ((ErrorValue) value).getCause());
    }

    @Test
    public void testClassCastCalculationError() {
        CellGrid grid = new CellGrid();
        GridUtils.addValueCell(grid, Pos.from("A1"), new DoubleValue(1));
        GridUtils.addValueCell(grid, Pos.from("B1"), new StringValue("abc"));

        List<Cell> cellList = new ArrayList<>();
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("B2"), "A1 + B1"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("B3"), "B1 - A1"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("B4"), "B1 * A1"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("B5"), "A1 / B1"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("B6"), "-B1"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("B5"), "A1 ^ B1"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("B5"), "B1 ^ A1"));

        for (Cell cell : cellList) {
            Value value = cell.getValue();
            assertTrue(value instanceof ErrorValue);
            assertEquals(ErrorValue.ErrorCode.COMPUTATION_ERROR.toString(), value.toString());
        }
    }

    @Test
    public void testIdentityCircularReferenceError() {
        CellGrid grid = new CellGrid();
        GridUtils.addFormulaCell(grid, Pos.from("A1"), "A1");

        Value value = grid.get(Pos.from("A1")).getValue();
        assertTrue(value instanceof ErrorValue);
        assertEquals(ErrorValue.ErrorCode.COMPUTATION_ERROR.toString(), value.toString());
        assertEquals("Circular dependency found in cell A1", ((ErrorValue) value).getCause());
    }

    @Test
    public void testErrorPropagation() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .build();

        List<Cell> cellList = new ArrayList<>();
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("B2"), "A1 + D5"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("C3"), "B2 * 3"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("D4"), "-B2"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("E5"), "3 ^ B2"));
        cellList.add(GridUtils.addFormulaCell(grid, Pos.from("F6"), "B2 ^ 1"));

        for (Cell cell : cellList) {
            Value value = cell.getValue();
            assertTrue(value instanceof ErrorValue);
            assertEquals(ErrorValue.ErrorCode.MISSING_CELL.toString(), value.toString());
        }
    }

    @Test
    public void testComplexCircularReferenceError() {
        CellGrid grid = GridBuilder.create()
                .withValueCell(Pos.from("A1"), new DoubleValue(1))
                .withValueCell(Pos.from("B2"), new DoubleValue(1))
                .withFormulaCell(Pos.from("C3"), "A1 + B2")
                .build();

        GridUtils.addFormulaCell(grid, Pos.from("B2"), "A1 + C3");

        Value value = grid.get(Pos.from("B2")).getValue();
        assertTrue(value instanceof ErrorValue);
        assertEquals(ErrorValue.ErrorCode.COMPUTATION_ERROR.toString(), value.toString());
        assertEquals("Circular dependency found in cell B2", ((ErrorValue) value).getCause());
    }

}
