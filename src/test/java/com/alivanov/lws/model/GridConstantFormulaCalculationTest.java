package com.alivanov.lws.model;

import com.alivanov.lws.expr.ComputationContext;
import com.alivanov.lws.expr.DoubleValue;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GridConstantFormulaCalculationTest {

    public static final String FORMULA_COMPUTATION_IS_NOT_CORRECT = "Formula computation is not correct";

    @Test
    public void testSimpleExpressionCalculation() {
        computeExpression("1", new DoubleValue(1));
        computeExpression("-1", new DoubleValue(-1));
        computeExpression("1 + 3", new DoubleValue(4));
        computeExpression("1 - 3", new DoubleValue(-2));
        computeExpression("-1 - 3", new DoubleValue(-4));
        computeExpression("1 * 3", new DoubleValue(3));
        computeExpression("2 / 3", new DoubleValue(2.0 / 3));
        computeExpression("2 + 3 * 3", new DoubleValue(11));
        computeExpression("2 / 3 * 2", new DoubleValue(4.0 / 3));
        computeExpression("2 * 2 + 3 * 3", new DoubleValue(13));
        computeExpression("1 * 3", new DoubleValue(3));
        computeExpression("1 * 3^3", new DoubleValue(27));
        computeExpression("0.5 * 2", new DoubleValue(1));
        computeExpression(".5 * 2", new DoubleValue(1));
    }

    @Test
    public void testNestedExpressionCalculation() {
        computeExpression("(1 + 2) * 3", new DoubleValue(9));
        computeExpression("((1 + 2)) * 3", new DoubleValue(9));
        computeExpression("((1 + 2) * 3)", new DoubleValue(9));
        computeExpression("-((1 + 2) * 3)", new DoubleValue(-9));
    }

    private void computeExpression(String formula, DoubleValue expected) {
        CellGrid grid = new CellGrid();
        Cell cell = GridUtils.addFormulaCellWithoutRecalculation(grid, Pos.from("A1"), formula);
        cell.compute(ComputationContext.createNew(grid));
        assertEquals(FORMULA_COMPUTATION_IS_NOT_CORRECT, expected, cell.getValue());
    }
}
