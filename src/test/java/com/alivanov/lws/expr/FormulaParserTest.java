package com.alivanov.lws.expr;

import com.alivanov.lws.model.Pos;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FormulaParserTest {

    public static final String RECOGNIZED_VARIABLES_ARE_NOT_CORRECT = "Recognized variables are not correct";

    private FormulaParser formulaParser;

    @Before
    public void init() {
        this.formulaParser = new FormulaParser();
    }

    @Test
    public void testValidAtoms() {
        List<String> validAtoms = new ArrayList<>();

        validAtoms.add("1");
        validAtoms.add("1.0");
        validAtoms.add("-1");
        validAtoms.add("-1.0");
        validAtoms.add("0.9");
        validAtoms.add("-0.9");
        validAtoms.add(".9");
        validAtoms.add("-.9");
        validAtoms.add("A1");
        validAtoms.add("A11");
        validAtoms.add("AAB1");
        validAtoms.add("AA112");

        for (String atom : validAtoms) {
            assertTrue("Formula \"" + atom + "\" should be valid in grammar", formulaParser.parseFormula(atom).isValid());
        }
    }

    @Test
    public void testInvalidAtoms() {
        List<String> invalidAtoms = new ArrayList<>();

        invalidAtoms.add("");
        invalidAtoms.add("$1");
        invalidAtoms.add("123AAB");

        for (String atom : invalidAtoms) {
            assertTrue("Formula \"" + atom + "\" should be invalid in grammar", !(formulaParser.parseFormula(atom).isValid()));
        }
    }

    @Test
    public void testValidFormulas() {
        List<String> validFormulas = new ArrayList<>();

        validFormulas.add("1 + 2");
        validFormulas.add("1 - 2");
        validFormulas.add("1 * 2");
        validFormulas.add("1 / 2");
        validFormulas.add("-1 / 2");
        validFormulas.add("123 - 32.1");
        validFormulas.add("123^2 - 32.1^2^3");
        validFormulas.add("(-b1 + (b2^2 -4*a3*c5)^0.50) / 4*a1*c2");

        for (String formula : validFormulas) {
            assertTrue("Formula \"" + formula + "\" should be valid in grammar", formulaParser.parseFormula(formula).isValid());
        }
    }

    @Test
    public void testInvalidFormulas() {
        List<String> invalidFormulas = new ArrayList<>();

        invalidFormulas.add("=");
        invalidFormulas.add("123 - 32,1");
        invalidFormulas.add("2^^2");

        for (String formula : invalidFormulas) {
            assertTrue("Formula \"" + formula + "\" should be invalid in grammar", !(formulaParser.parseFormula(formula).isValid()));
        }
    }

    @Test
    public void testVariablesRecognition() {
        Map<String, Pos> expected;

        expected = new HashMap<>();
        Formula noVariablesFormula = formulaParser.parseFormula("2 + 3");
        assertEquals(RECOGNIZED_VARIABLES_ARE_NOT_CORRECT, expected, noVariablesFormula.getVariables());

        expected = new HashMap<>();
        expected.put("A1", Pos.from("A1"));
        Formula oneVariableFormula = formulaParser.parseFormula("2 + A1");
        assertEquals(RECOGNIZED_VARIABLES_ARE_NOT_CORRECT, expected, oneVariableFormula.getVariables());

        expected = new HashMap<>();
        expected.put("A1", Pos.from("A1"));
        expected.put("A2", Pos.from("A2"));
        expected.put("A3", Pos.from("A3"));
        expected.put("AB14", Pos.from("AB14"));
        expected.put("B1", Pos.from("B1"));
        Formula severalVariableFormula = formulaParser.parseFormula("B1 + A1 * A2 - (A3 + AB14)");
        assertEquals(RECOGNIZED_VARIABLES_ARE_NOT_CORRECT, expected, severalVariableFormula.getVariables());

        expected = new HashMap<>();
        expected.put("A1", Pos.from("A1"));
        expected.put("A2", Pos.from("A2"));
        expected.put("A3", Pos.from("A3"));
        expected.put("B1", Pos.from("B1"));
        Formula duplicateVariablesFormula = formulaParser.parseFormula("B1 + A1 ^ A2 - (A3 * B1)");
        assertEquals(RECOGNIZED_VARIABLES_ARE_NOT_CORRECT, expected, duplicateVariablesFormula.getVariables());

        expected = new HashMap<>();
        Formula errorVariableFormula = formulaParser.parseFormula("2 + A");
        assertEquals(RECOGNIZED_VARIABLES_ARE_NOT_CORRECT, expected, errorVariableFormula.getVariables());
    }
}