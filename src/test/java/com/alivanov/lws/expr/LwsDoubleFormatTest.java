package com.alivanov.lws.expr;

import org.junit.Test;

import java.text.ParseException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class LwsDoubleFormatTest {

    @Test
    public void testFormat() {
        String format;

        format = LwsDoubleFormat.format(1.1);
        assertEquals("1.1", format);

        format = LwsDoubleFormat.format(1.0);
        assertEquals("1", format);

        format = LwsDoubleFormat.format(0.1);
        assertEquals("0.1", format);

        format = LwsDoubleFormat.format(1.0 / 4);
        assertEquals("0.25", format);

        format = LwsDoubleFormat.format(1.0 / 3);
        String expected = "0." + IntStream.range(0, LwsDoubleFormat.MAXIMUM_DIGITS).mapToObj(i -> "3").collect(Collectors.joining(""));
        assertEquals(expected, format);
    }

    @Test
    public void testSuccessfulParse() throws ParseException {
        Double expected;

        expected = 1.0;
        assertTrue(expected.equals(LwsDoubleFormat.parse("1")));

        expected = 1.1;
        assertTrue(expected.equals(LwsDoubleFormat.parse("1.1")));

        expected = 0.1;
        assertTrue(expected.equals(LwsDoubleFormat.parse("0.1")));

        expected = 1.1234567890123;
        assertTrue(expected.equals(LwsDoubleFormat.parse("1.1234567890123")));
    }

    @Test(expected = ParseException.class)
    public void testErrorParseString() throws ParseException {
        LwsDoubleFormat.parse("abc123");
        fail();
    }

    @Test(expected = ParseException.class)
    public void testErrorParseCommaValue() throws ParseException {
        LwsDoubleFormat.parse("1,1");
        fail();
    }

    @Test(expected = ParseException.class)
    public void testErrorNumWithString() throws ParseException {
        LwsDoubleFormat.parse("123abc");
        fail();
    }
}