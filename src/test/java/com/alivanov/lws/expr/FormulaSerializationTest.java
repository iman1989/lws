package com.alivanov.lws.expr;

import org.junit.Test;

import java.io.IOException;

import static com.alivanov.lws.Utils.restore;
import static com.alivanov.lws.Utils.serialize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class FormulaSerializationTest {

    public static final String FORMULA_STRINGS_ARE_DIFFERENT = "Formula strings are different";
    public static final String PARSED_FORMULA_IS_CORRUPT = "Parsed formula is corrupt";

    @Test
    public void testSaveAndRestoreValidFormula() throws IOException, ClassNotFoundException {
        Formula formula = new FormulaParser().parseFormula("A1 + B2");

        byte[] bytes = serialize(formula);
        Formula restored = (Formula) restore(bytes);

        // Check if formula strings are the same
        assertEquals(FORMULA_STRINGS_ARE_DIFFERENT, formula, restored);

        // Check that formula was parsed again during read object stream
        assertEquals(PARSED_FORMULA_IS_CORRUPT, formula.isValid(), restored.isValid());
        assertNotNull(PARSED_FORMULA_IS_CORRUPT, restored.getParseTree());
        assertEquals(PARSED_FORMULA_IS_CORRUPT, formula.getVariables(), restored.getVariables());
    }

    @Test
    public void testSaveAndRestoreInvalidFormula() throws IOException, ClassNotFoundException {
        Formula formula = new FormulaParser().parseFormula("A =-+ B2");

        byte[] bytes = serialize(formula);
        Formula restored = (Formula) restore(bytes);

        // Check if formula strings are the same
        assertEquals(FORMULA_STRINGS_ARE_DIFFERENT, formula, restored);

        // Check that formula was parsed again during read object stream
        assertEquals(PARSED_FORMULA_IS_CORRUPT, formula.isValid(), restored.isValid());
        assertNull(PARSED_FORMULA_IS_CORRUPT, restored.getParseTree());
        assertEquals(PARSED_FORMULA_IS_CORRUPT, formula.getVariables(), restored.getVariables());
    }

}
