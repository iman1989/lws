package com.alivanov.lws.model;

import com.alivanov.lws.expr.*;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

class FormulaCell extends Cell {

    public static final String FORMULA_PREFIX = "=";

    private final Formula formula;
    private boolean invalidated;

    FormulaCell(Pos pos, Formula formula) {
        super(pos);

        this.formula = formula;
        this.invalidated = true;
    }

    @Override
    protected void setValue(Value value) {
        this.value = value;
        this.invalidated = false;
    }

    /*
    Recursively computes a formula cell using a visitor pattern
    Will exit immediately if:
        - Value is already computed, and this cell is valid;
        - Formula is parsed not correctly;
        - This cell was already visited in this branch of recursion,
        which signals of a circular dependency.
    */
    @Override
    public Value compute(ComputationContext ctx) {
        if (!invalidated) {
            return getValue();
        }

        Value result;
        if (!formula.isValid()) {
            result = new ErrorValue(ErrorValue.ErrorCode.INVALID_FORMULA,
                    "Entered formula is invalid");
        } else if (!updateVisitedCells(ctx, getPos())) {
            result = new ErrorValue(ErrorValue.ErrorCode.COMPUTATION_ERROR,
                    "Circular dependency found in cell " + getPos().toSheetCoordinates());
        } else {
            LwsVisitor<Value> visitor = new ComputeExpressionVisitor(ctx, formula.getVariables());
            try {
                result = visitor.visit(formula.getParseTree());
            } catch (Exception e) {
                result = new ErrorValue(ErrorValue.ErrorCode.COMPUTATION_ERROR, e.getMessage());
            }
        }

        setValue(result);
        return result;
    }

    private boolean updateVisitedCells(ComputationContext ctx, Pos pos) {
        return ctx.getVisitedCells().add(pos);
    }

    @Override
    public String getEditorView() {
        return FORMULA_PREFIX + formula.getFormulaText();
    }

    @Override
    public void invalidate() {
        this.invalidated = true;
    }

    @Override
    public Set<Pos> getParentPositions() {
        return new HashSet<>(formula.getVariables().values());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FormulaCell that = (FormulaCell) o;
        return invalidated == that.invalidated &&
                Objects.equals(formula, that.formula);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), formula, invalidated);
    }

    @Override
    public String toString() {
        return "FormulaCell{" +
                "pos=" + pos +
                ", value=" + value +
                ", formula=" + formula +
                ", invalidated=" + invalidated +
                '}';
    }

    protected Formula getFormula() {
        return this.formula;
    }
}
