package com.alivanov.lws.model;

import java.io.Serializable;
import java.util.Objects;

public final class Pos implements Serializable {

    private final int row;
    private final int col;

    private Pos(int row, int col) {
        this.row = row;
        this.col = col;
    }

    public static Pos from(int row, int col) {
        return new Pos(row, col);
    }

    public static Pos from(String rawSheetCoordinates) {
        if (rawSheetCoordinates == null) {
            return null;
        }

        rawSheetCoordinates = rawSheetCoordinates.toUpperCase();

        // TODO Regexp compile and match column-row syntax

        int i = 0;
        while (!Character.isDigit(rawSheetCoordinates.charAt(i))) {
            i++;
        }

        int row = Integer.parseInt(rawSheetCoordinates.substring(i, rawSheetCoordinates.length())) - 1;
        int col = convertColumn(rawSheetCoordinates.substring(0, i));

        return new Pos(row, col);
    }

    private static int convertColumn(String column) {
        int pow = 0;
        int result = 0;

        int i = column.length();

        while (i > 0) {
            int offset = column.charAt(i - 1) - 'A';
            result += offset + (26 * pow);

            i--;
            pow++;
        }

        return result;
    }

    public String toSheetCoordinates() {
        String result = "";
        for (int column = col; column >= 0; column = column / 26 - 1) {
            result = (char) ((char) (column % 26) + 'A') + result;
        }

        return result + Integer.toString(row + 1);
    }

    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pos pos = (Pos) o;
        return row == pos.row &&
                col == pos.col;
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }

    @Override
    public String toString() {
        return "(" + row + ", " + col + ")";
    }
}
