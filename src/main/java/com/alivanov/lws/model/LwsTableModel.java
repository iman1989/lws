package com.alivanov.lws.model;

import com.alivanov.lws.expr.*;

import javax.swing.event.TableModelEvent;
import javax.swing.table.AbstractTableModel;
import java.text.ParseException;

public class LwsTableModel extends AbstractTableModel {

    private int rowCount;
    private int colCount;

    private CellGrid grid;

    public LwsTableModel(int rowCount, int colCount) {
        this.rowCount = rowCount;
        this.colCount = colCount;

        grid = new CellGrid();
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int getColumnCount() {
        return colCount;
    }

    public LwsSheet saveToSheet() {
        return new LwsSheet(this.rowCount, this.colCount, this.grid);
    }

    public void loadFromSheet(LwsSheet sheet) {
        this.rowCount = sheet.getMaxRowCount();
        this.colCount = sheet.getMaxColCount();
        this.grid = sheet.getGrid();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return Cell.class;
    }

    @Override
    public Cell getValueAt(int rowIndex, int columnIndex) {
        return grid.get(rowIndex, columnIndex);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (aValue != null && !(aValue instanceof String)) {
            return;
        }

        final String stringValue = (String) aValue;
        final Cell cell = grid.get(rowIndex, columnIndex);

        if (stringValue == null || stringValue.equals("")) {
            if (cell != null) {
                grid.remove(rowIndex, columnIndex);
                fireTableDataChanged();
            }

            return;
        }
        if (cell != null && cell.getEditorView().equals(stringValue)) {
            return;
        }

        Cell newCell;
        if (stringValue.length() > 1 && stringValue.startsWith(FormulaCell.FORMULA_PREFIX)) {
            Formula formula = new FormulaParser().parseFormula(stringValue.substring(1));
            newCell = new FormulaCell(Pos.from(rowIndex, columnIndex), formula);
        } else {
            try {
                double doubleValue = LwsDoubleFormat.parse(stringValue);
                newCell = new ValueCell(Pos.from(rowIndex, columnIndex), new DoubleValue(doubleValue));
            } catch (ParseException e) {
                newCell = new ValueCell(Pos.from(rowIndex, columnIndex), new StringValue(stringValue));
            }
        }

        grid.write(newCell);
        fireTableDataChanged();
    }

    // This method is overridden not to lose table selection on fireTableDataChanged() events
    @Override
    public void fireTableDataChanged() {
        super.fireTableChanged(new TableModelEvent(
                this,
                0,
                getRowCount() - 1,
                TableModelEvent.ALL_COLUMNS,
                TableModelEvent.UPDATE));
    }

    protected CellGrid getGrid() {
        return this.grid;
    }
}
