package com.alivanov.lws.model;

import com.alivanov.lws.expr.ComputationContext;
import com.alivanov.lws.expr.Value;

import java.util.Collections;
import java.util.Set;

class ValueCell extends Cell {

    ValueCell(Pos pos, Value value) {
        super(pos);
        setValue(value);
    }

    @Override
    public Value compute(ComputationContext ctx) {
        return getValue();
    }

    @Override
    public String getEditorView() {
        return getValue().toString();
    }

    @Override
    public void invalidate() {
        // Do nothing - value cells are always up-to-date
    }

    @Override
    public Set<Pos> getParentPositions() {
        return Collections.emptySet();
    }

    @Override
    public String toString() {
        return "ValueCell{" +
                "pos=" + pos +
                ", value=" + value +
                '}';
    }
}
