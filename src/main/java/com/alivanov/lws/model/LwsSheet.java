package com.alivanov.lws.model;

import java.io.*;
import java.util.Objects;

public class LwsSheet implements Serializable {

    private final int maxRowCount;
    private final int maxColCount;

    private final CellGrid grid;

    public LwsSheet(int maxRowCount, int maxColCount) {
        this.maxRowCount = maxRowCount;
        this.maxColCount = maxColCount;

        this.grid = new CellGrid();
    }

    public LwsSheet(int maxRowCount, int maxColCount, CellGrid grid) {
        this.maxRowCount = maxRowCount;
        this.maxColCount = maxColCount;

        this.grid = grid;
    }

    public int getMaxRowCount() {
        return maxRowCount;
    }

    public int getMaxColCount() {
        return maxColCount;
    }

    public CellGrid getGrid() {
        return grid;
    }

    public static boolean saveWorksheet(LwsSheet sheet, File file) {
        try (
                FileOutputStream fout = new FileOutputStream(file);
                ObjectOutputStream oos = new ObjectOutputStream(fout)
        ) {
            oos.writeObject(sheet);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static LwsSheet openWorksheet(File file) throws Exception {
        try (
                FileInputStream fin = new FileInputStream(file);
                ObjectInputStream ois = new ObjectInputStream(fin)
        ) {
            return (LwsSheet) ois.readObject();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LwsSheet lwsSheet = (LwsSheet) o;
        return maxRowCount == lwsSheet.maxRowCount &&
                maxColCount == lwsSheet.maxColCount &&
                Objects.equals(grid, lwsSheet.grid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maxRowCount, maxColCount, grid);
    }

    @Override
    public String toString() {
        return "LwsSheet{" +
                "maxRowCount=" + maxRowCount +
                ", maxColCount=" + maxColCount +
                ", grid=" + grid.toString() +
                '}';
    }
}
