package com.alivanov.lws.model;

import com.alivanov.lws.expr.ComputationContext;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class CellGrid implements Serializable {

    private final Map<Pos, Cell> data;
    private final Map<Pos, Set<Pos>> dependencyGraph;

    CellGrid() {
        data = new HashMap<>();
        dependencyGraph = new HashMap<>();
    }

    public Cell get(int row, int col) {
        return get(Pos.from(row, col));
    }

    public Cell get(Pos pos) {
        return data.get(pos);
    }

    public int size() {
        return data.size();
    }

    public Cell write(Cell cell) {
        Pos pos = cell.getPos();

        Cell oldCell = data.get(pos);
        if (oldCell != null) {
            removeDependencyFromParents(oldCell);
        }

        addDependencyToParents(cell);
        data.put(pos, cell);
        recalculateDependencies(cell, true);

        return cell;
    }

    public Cell remove(int row, int col) {
        return remove(Pos.from(row, col));
    }

    public Cell remove(Pos pos) {
        Cell cell = data.get(pos);

        if (cell != null) {
            removeDependencyFromParents(cell);
            data.remove(pos);
            recalculateDependencies(cell, false);
        }

        return cell;
    }

    /*
    Fires the recalculation process of all dependencies, possibly including changed cell itself
    Consists of three steps:
        - Construct a set of cells to be recomputed
        - Mark them as invalidated, not to use their old value anymore
        - For each of those cells, perform a recursive calculation of their values
     */
    private void recalculateDependencies(Cell cell, boolean including) {
        Set<Pos> positionsToRecompute = new LinkedHashSet<>();
        if (including) {
            positionsToRecompute.add(cell.getPos());
        }
        addDependenciesForRecompute(positionsToRecompute, cell.getPos());

        List<Cell> cellsToRecompute = positionsToRecompute.stream().map(data::get).collect(Collectors.toList());
        cellsToRecompute.forEach(Cell::invalidate);
        cellsToRecompute.forEach(c -> c.compute(ComputationContext.createNew(this)));
    }

    /*
        Construct a set of cells to be recomputed by traversing a dependency graph using BFS.
        BFS and a linked set are used to minimize amount of recursive calculations on the next step.
        Will skip a cell if it is already visited, and does not detect cyclic references.
     */
    private void addDependenciesForRecompute(Set<Pos> positionsToRecompute, Pos pos) {
        Queue<Pos> queue = new LinkedList<>(getDependencies(pos));

        while (!queue.isEmpty()) {
            Pos currentPos = queue.poll();
            if (!positionsToRecompute.contains(currentPos)) {
                positionsToRecompute.add(currentPos);
                queue.addAll(getDependencies(currentPos));
            }
        }
    }

    Set<Pos> getDependencies(Pos of) {
        return dependencyGraph.getOrDefault(of, new HashSet<>());
    }

    void addDependencyToParents(Cell cell) {
        cell.getParentPositions().forEach(parent -> addDependency(parent, cell.getPos()));
    }

    private void addDependency(Pos from, Pos to) {
        dependencyGraph.computeIfAbsent(from, pos -> new HashSet<>()).add(to);
    }

    private void removeDependencyFromParents(Cell cell) {
        cell.getParentPositions().forEach(parent -> removeDependency(parent, cell.getPos()));
    }

    private void removeDependency(Pos from, Pos to) {
        Set<Pos> dependencies = dependencyGraph.get(from);

        if (dependencies != null) {
            dependencies.remove(to);
            if (dependencies.size() == 0) {
                dependencyGraph.remove(from);
            }
        }
    }

    Map<Pos, Cell> getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CellGrid cellGrid = (CellGrid) o;
        return Objects.equals(data, cellGrid.data) &&
                Objects.equals(dependencyGraph, cellGrid.dependencyGraph);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, dependencyGraph);
    }

    @Override
    public String toString() {
        return "CellGrid{" +
                "data=" + data +
                ", dependencyGraph=" + dependencyGraph +
                '}';
    }
}
