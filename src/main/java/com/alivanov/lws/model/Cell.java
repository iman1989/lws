package com.alivanov.lws.model;

import com.alivanov.lws.expr.ComputationContext;
import com.alivanov.lws.expr.Value;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

public abstract class Cell implements Serializable {

    protected final Pos pos;
    protected Value value;

    public Cell(Pos pos) {
        this.pos = pos;
    }

    public Pos getPos() {
        return pos;
    }

    public Value getValue() {
        return value;
    }

    public abstract Value compute(ComputationContext ctx);

    public abstract String getEditorView();

    public abstract void invalidate();

    public abstract Set<Pos> getParentPositions();

    protected void setValue(Value value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return Objects.equals(pos, cell.pos) &&
                Objects.equals(value, cell.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pos, value);
    }
}
