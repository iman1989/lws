package com.alivanov.lws.ui;

import com.alivanov.lws.expr.ErrorValue;
import com.alivanov.lws.model.Cell;
import com.alivanov.lws.model.LwsSheet;
import com.alivanov.lws.model.LwsTableModel;
import com.alivanov.lws.model.Pos;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

import static com.alivanov.lws.ui.LwsApplicationProperties.*;

public class LwsApplication {

    private static final LwsApplication INSTANCE = new LwsApplication();

    private JFrame mainFrame;

    private JPanel tablePanel;
    private LwsTable table;

    private JTextField cellCoordinatesField;
    private JTextField formulaEditorField;

    private JLabel statusText;

    private File openedFile;

    private LwsApplication() {
    }

    public static LwsApplication getInstance() {
        return INSTANCE;
    }

    public void initAndRenderUI() {
        mainFrame = constructFrame();

        mainFrame.setJMenuBar(constructMenuBar());
        mainFrame.add(LwsToolbarBuilder.constructToolBar(this), BorderLayout.NORTH);
        mainFrame.add(LwsTableBuilder.constructTablePanel(this));
        mainFrame.add(LwsStatusBarBuilder.constructStatusBar(this), BorderLayout.SOUTH);

        initListeners();

        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        table.initFocus();
        mainFrame.setVisible(true);
    }

    private JFrame constructFrame() {
        JFrame frame = new JFrame(LWS_NEW_DOCUMENT_TITLE);

        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (JOptionPane.showConfirmDialog(frame, CONFIRM_APPLICATION_CLOSING, LWS_TITLE,
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });

        return frame;
    }

    private JMenuBar constructMenuBar() {
        final JMenuBar menuBar = new JMenuBar();

        final JMenu fileMenu = new JMenu("File");

        final JMenuItem newMenuItem = new JMenuItem("New", KeyEvent.VK_N);
        newMenuItem.addActionListener(new CreateWorkSheetAction());
        fileMenu.add(newMenuItem);

        final JMenuItem openMenuItem = new JMenuItem("Open", KeyEvent.VK_O);
        openMenuItem.addActionListener(new OpenAction());
        fileMenu.add(openMenuItem);

        final JMenuItem saveMenuItem = new JMenuItem("Save", KeyEvent.VK_S);
        saveMenuItem.addActionListener(new SaveAction());
        fileMenu.add(saveMenuItem);

        final JMenuItem saveAsMenuItem = new JMenuItem("Save As", KeyEvent.VK_A);
        saveAsMenuItem.addActionListener(new SaveAsAction());
        fileMenu.add(saveAsMenuItem);

        fileMenu.addSeparator();

        final JMenuItem exitMenuItem = new JMenuItem("Exit", KeyEvent.VK_E);
        exitMenuItem.addActionListener(new ExitAction());
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);
        return menuBar;
    }

    private void initListeners() {
        ListSelectionListener updateFormulaFieldSelectionListener = new UpdateFormulaFieldSelectionListener();
        table.getSelectionModel().addListSelectionListener(updateFormulaFieldSelectionListener);
        table.getColumnModel().getSelectionModel().addListSelectionListener(updateFormulaFieldSelectionListener);

        // Change table model
        table.getModel().addTableModelListener(e -> {
            updateFormulaTextField();
            updateStatusText();
        });

        cellCoordinatesField.addActionListener(new TableCellInputAction());

        formulaEditorField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                super.focusGained(e);
                table.editCellAt(table.getSelectedRow(), table.getSelectedColumn());
            }
        });

        formulaEditorField.addActionListener(e -> {
            if (table.isEditing()) {
                table.getCellEditor().stopCellEditing();
            }
            table.requestFocus();
        });

        // Change cell contents only if formula bar is focused to avoid infinite loop from both document listeners
        formulaEditorField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (formulaEditorField.hasFocus() && table.isEditing()) {
                    ((JTextField) table.getEditorComponent()).setText(formulaEditorField.getText());
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (formulaEditorField.hasFocus() && table.isEditing()) {
                    ((JTextField) table.getEditorComponent()).setText(formulaEditorField.getText());
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
    }

    private class UpdateFormulaFieldSelectionListener implements ListSelectionListener {
        @Override
        public void valueChanged(final ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) {
                updateCellCoordinatesField();
                updateFormulaTextField();
                updateStatusText();
            }
        }
    }

    private class TableCellInputAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            Pos pos;
            try {
                pos = Pos.from(cellCoordinatesField.getText());
            } catch (NumberFormatException ex) {
                return;
            }

            if (pos.getRow() < 0 || pos.getRow() >= table.getModel().getRowCount() ||
                    pos.getCol() < 0 || pos.getCol() >= table.getModel().getColumnCount()) {
                return;
            }

            table.requestFocus();
            table.changeSelection(pos.getRow(), pos.getCol(), false, false);
        }
    }

    private void updateCellCoordinatesField() {
        cellCoordinatesField.setText(
                Pos.from(table.getSelectedRow(),
                        table.getSelectedColumn()).toSheetCoordinates());
    }

    private void updateFormulaTextField() {
        Cell cell = (Cell) table.getValueAt(table.getSelectedRow(), table.getSelectedColumn());
        this.getFormulaEditorField().setText(cell == null ? "" : cell.getEditorView());
    }

    private void updateStatusText() {
        Cell cell = (Cell) table.getValueAt(table.getSelectedRow(), table.getSelectedColumn());
        if (cell != null && cell.getValue() instanceof ErrorValue) {
            this.statusText.setText(((ErrorValue) cell.getValue()).getCause());
        } else {
            this.statusText.setText("");
        }
    }

    private class CreateWorkSheetAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (JOptionPane.showConfirmDialog(mainFrame, YOU_WILL_LOSE_UNSAVED_PROGRESS_CONTINUE, LWS_TITLE,
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION) {
                final Dimension dimension = NewWorksheetDialog.showDialog(mainFrame);
                if (dimension != null) {
                    createEmptyWorksheet(dimension.width, dimension.height);
                }

                showFileAsOpened(null);
            }
        }
    }

    private class OpenAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (JOptionPane.showConfirmDialog(mainFrame, YOU_WILL_LOSE_UNSAVED_PROGRESS_CONTINUE, LWS_TITLE,
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION) {
                final JFileChooser fileChooser = createLwsFileChooser();
                final int returnVal = fileChooser.showOpenDialog(mainFrame);

                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    final File file = fileChooser.getSelectedFile();
                    final JDialog waitDialog = createPleaseWaitDialog();

                    /*
                    All formulas are parsed again during deserialization, so we should load
                    only in background thread
                     */
                    new OpenFileWorker(file, waitDialog).execute();
                    waitDialog.setVisible(true);
                }
            }
        }
    }

    private JDialog createPleaseWaitDialog() {
        final JDialog modalDialog = new JDialog(mainFrame, "Please wait...", Dialog.ModalityType.DOCUMENT_MODAL);

        modalDialog.setUndecorated(true);
        modalDialog.setLocationRelativeTo(mainFrame);
        final JLabel pleaseWaitLabel = new JLabel("Please wait...");
        modalDialog.add(pleaseWaitLabel);
        modalDialog.pack();

        return modalDialog;
    }

    private class OpenFileWorker extends SwingWorker<LwsSheet, Void> {

        private final File file;
        private final JDialog dialog;

        public OpenFileWorker(File file, JDialog dialog) {
            this.file = file;
            this.dialog = dialog;
        }

        @Override
        protected LwsSheet doInBackground() throws Exception {
            return LwsSheet.openWorksheet(file);
        }

        @Override
        protected void done() {
            try {
                final LwsSheet sheet = get();
                loadWorkSheet(sheet);
                showFileAsOpened(file);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(mainFrame,
                        "An error occurred during file opening: " + e.getMessage(),
                        "Error during file opening",
                        JOptionPane.ERROR_MESSAGE);
            }
            dialog.dispose();
        }
    }

    private class SaveAction extends SaveAsAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (openedFile != null) {
                LwsSheet.saveWorksheet(table.getModel().saveToSheet(), openedFile);
            } else {
                super.actionPerformed(e);
            }
        }
    }

    private class SaveAsAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            final JFileChooser fileChooser = createLwsFileChooser();
            final int returnVal = fileChooser.showSaveDialog(mainFrame);

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                if (!file.getName().toLowerCase().endsWith(LWS_FILE_EXTENSION)) {
                    file = new File(file.getPath() + LWS_FILE_EXTENSION);
                }
                LwsSheet.saveWorksheet(table.getModel().saveToSheet(), file);
                showFileAsOpened(file);
            }
        }
    }

    private static class ExitAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            final Window window = KeyboardFocusManager.getCurrentKeyboardFocusManager().getActiveWindow();
            if (window != null) {
                final WindowEvent windowClosing = new WindowEvent(window, WindowEvent.WINDOW_CLOSING);
                window.dispatchEvent(windowClosing);
            }
        }
    }

    private JFileChooser createLwsFileChooser() {
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter lwsFilter = new FileNameExtensionFilter("LWS files (*.lws)", "lws");
        fileChooser.addChoosableFileFilter(lwsFilter);
        fileChooser.setFileFilter(lwsFilter);
        return fileChooser;
    }

    private void createEmptyWorksheet(int rows, int cols) {
        loadWorkSheet(new LwsSheet(rows, cols));
    }

    private void loadWorkSheet(LwsSheet sheet) {
        LwsTableModel model = table.getModel();

        model.loadFromSheet(sheet);
        model.fireTableStructureChanged();

        LwsTableBuilder.redrawTablePanel(tablePanel, table);
        table.initFocus();
    }

    private void showFileAsOpened(File file) {
        openedFile = file;
        if (file == null) {
            mainFrame.setTitle(LWS_NEW_DOCUMENT_TITLE);
        } else {
            mainFrame.setTitle(LWS_MAIN_FRAME_TITLE + file.getName());
        }
    }

    public JTextField getFormulaEditorField() {
        return formulaEditorField;
    }

    public void setCellField(JTextField cellField) {
        this.cellCoordinatesField = cellField;
    }

    public void setFormulaEditorField(JTextField formulaEditorField) {
        this.formulaEditorField = formulaEditorField;
    }

    public void setTablePanel(JPanel tablePanel) {
        this.tablePanel = tablePanel;
    }

    public void setTable(LwsTable table) {
        this.table = table;
    }

    public void setStatusText(JLabel statusText) {
        this.statusText = statusText;
    }
}