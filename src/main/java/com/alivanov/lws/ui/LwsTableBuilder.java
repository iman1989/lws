package com.alivanov.lws.ui;

import com.alivanov.lws.model.LwsTableModel;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import static com.alivanov.lws.ui.LwsApplicationProperties.DEFAULT_COLUMN_COUNT;
import static com.alivanov.lws.ui.LwsApplicationProperties.DEFAULT_ROW_COUNT;

class LwsTableBuilder {

    static JPanel constructTablePanel(LwsApplication application) {
        final JPanel panel = new JPanel(new BorderLayout());

        final LwsTableModel tableModel = new LwsTableModel(DEFAULT_ROW_COUNT, DEFAULT_COLUMN_COUNT);
        final LwsTable table = new LwsTable(tableModel);
        constructScrollPane(panel, table);
        addPopupMenu(table);

        application.setTablePanel(panel);
        application.setTable(table);
        return panel;
    }

    private static void addPopupMenu(final LwsTable table) {
        final JPopupMenu popupMenu = new JPopupMenu();

        final JMenuItem copyMenuItem = new JMenuItem("Copy");
        copyMenuItem.addActionListener(table.getActionMap().get("Copy"));
        popupMenu.add(copyMenuItem);

        final JMenuItem cutMenuItem = new JMenuItem("Cut");
        cutMenuItem.addActionListener(table.getActionMap().get("Cut"));
        popupMenu.add(cutMenuItem);

        final JMenuItem pasteMenuItem = new JMenuItem("Paste");
        pasteMenuItem.addActionListener(table.getActionMap().get("Paste"));
        popupMenu.add(pasteMenuItem);

        final MouseListener rightClickListener = new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                selectCell(e);
            }

            @Override
            public void mouseReleased(final MouseEvent e) {
                selectCell(e);
            }

            private void selectCell(final MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    final int row = table.rowAtPoint(e.getPoint());
                    final int col = table.columnAtPoint(e.getPoint());
                    if (row != -1 && col != -1) {
                        table.changeSelection(row, col, false, false);
                    }
                }
            }
        };
        table.setComponentPopupMenu(popupMenu);
        table.addMouseListener(rightClickListener);
    }

    static void redrawTablePanel(JPanel panel, JTable table) {
        panel.removeAll();

        constructScrollPane(panel, table);

        panel.revalidate();
        panel.repaint();
    }

    private static void constructScrollPane(JPanel panel, JTable table) {
        final JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setRowHeader(constructRowHeaders(table));
        panel.add(scrollPane, BorderLayout.CENTER);
    }

    private static JViewport constructRowHeaders(JTable table) {
        final JViewport vp = new JViewport();

        final JPanel pnl = new JPanel(null);
        final Dimension dim = new Dimension(30, 200);
        pnl.setPreferredSize(dim);

        for (int i = 0; i < table.getRowCount(); i++) {
            final JLabel lbl = new JLabel(Integer.toString(i + 1), SwingConstants.CENTER);

            lbl.setFont(table.getTableHeader().getFont());
            lbl.setBorder((Border) UIManager.getDefaults().get("TableHeader.cellBorder"));
            lbl.setBounds(0, i * table.getRowHeight(), dim.width, table.getRowHeight());
            pnl.add(lbl);
        }

        dim.height = table.getRowHeight() * table.getRowCount();

        vp.setViewSize(dim);
        vp.setView(pnl);

        return vp;
    }

}
