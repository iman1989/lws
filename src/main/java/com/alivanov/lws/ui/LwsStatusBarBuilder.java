package com.alivanov.lws.ui;

import javax.swing.*;
import java.awt.*;

import static com.alivanov.lws.ui.LwsApplicationProperties.LWS_STATUS_FONT;

class LwsStatusBarBuilder {

    static JPanel constructStatusBar(LwsApplication application) {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.setPreferredSize(new Dimension(200, 20));
        panel.setBorder(BorderFactory.createLoweredBevelBorder());

        final JLabel statusText = new JLabel("", SwingConstants.LEFT);
        statusText.setFont(LWS_STATUS_FONT);
        statusText.setBorder(BorderFactory.createCompoundBorder(statusText.getBorder(),
                BorderFactory.createEmptyBorder(0, 5, 0, 5)));

        application.setStatusText(statusText);

        panel.add(statusText, BorderLayout.CENTER);
        return panel;
    }

}
