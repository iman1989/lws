package com.alivanov.lws.ui;

import javax.swing.*;
import java.awt.*;

import static com.alivanov.lws.ui.LwsApplicationProperties.*;

class NewWorksheetDialog extends JDialog {

    private Dimension value;

    private final JSpinner rows;
    private final JSpinner cols;

    static Dimension showDialog(JFrame frame) {
        final NewWorksheetDialog dialog = new NewWorksheetDialog(frame);
        dialog.setVisible(true);
        return dialog.value;
    }

    private NewWorksheetDialog(Frame frame) {
        super(frame, LWS_CREATE_NEW_WORKSHEET_TITLE, true);

        SpinnerModel rowSpinnerModel = new SpinnerNumberModel(DEFAULT_ROW_COUNT, MIN_ROW_COUNT, MAX_ROW_COUNT, 1);
        SpinnerModel columnSpinnerModel = new SpinnerNumberModel(DEFAULT_COLUMN_COUNT, MIN_COLUMN_COUNT, MAX_COLUMN_COUNT, 1);

        rows = new JSpinner(rowSpinnerModel);
        cols = new JSpinner(columnSpinnerModel);

        final JButton okButton = new JButton("Ok");
        okButton.setActionCommand("Ok");
        okButton.addActionListener(e -> {
            int rowCount = (int) rows.getValue();
            int colCount = (int) cols.getValue();

            value = new Dimension(rowCount, colCount);
            NewWorksheetDialog.this.setVisible(false);
        });
        getRootPane().setDefaultButton(okButton);

        final JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> NewWorksheetDialog.this.setVisible(false));

        final JPanel textFieldPane = new JPanel();
        textFieldPane.setLayout(new BoxLayout(textFieldPane, BoxLayout.PAGE_AXIS));
        textFieldPane.add(Box.createVerticalGlue());
        final JLabel labelRows = new JLabel("Rows");
        labelRows.setLabelFor(rows);
        textFieldPane.add(labelRows);
        textFieldPane.add(rows);
        textFieldPane.add(Box.createRigidArea(new Dimension(0, 5)));
        final JLabel labelCols = new JLabel("Columns");
        labelCols.setLabelFor(cols);
        textFieldPane.add(labelCols);
        textFieldPane.add(cols);
        textFieldPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        final JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.LINE_AXIS));
        buttonPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 10, 10));
        buttonPane.add(Box.createHorizontalGlue());
        buttonPane.add(okButton);
        buttonPane.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonPane.add(cancelButton);

        final Container contentPane = getContentPane();
        contentPane.add(textFieldPane, BorderLayout.CENTER);
        contentPane.add(buttonPane, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(frame);
    }
}
