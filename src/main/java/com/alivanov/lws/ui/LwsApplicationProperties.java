package com.alivanov.lws.ui;

import java.awt.*;

public class LwsApplicationProperties {

    public static final String LWS_TITLE = "LWS";
    public static final String LWS_MAIN_FRAME_TITLE = LWS_TITLE + " - ";
    public static final String LWS_NEW_DOCUMENT_TITLE = LWS_TITLE + " - New Document";

    public static final String LWS_CREATE_NEW_WORKSHEET_TITLE = "New worksheet";

    public static final String CONFIRM_APPLICATION_CLOSING = "Are you sure you want to close the application?";
    public static final String YOU_WILL_LOSE_UNSAVED_PROGRESS_CONTINUE = "You will lose unsaved progress. Continue?";

    public static final String LWS_FILE_EXTENSION = ".lws";

    public static final Font LWS_CONTENT_FONT = new Font("Calibri", Font.PLAIN, 15);
    public static final Font LWS_HEADER_FONT = new Font("Calibri", Font.PLAIN, 15);
    public static final Font LWS_STATUS_FONT = new Font("Calibri", Font.PLAIN, 12);

    public static final int DEFAULT_ROW_COUNT = 50;
    public static final int DEFAULT_COLUMN_COUNT = 50;

    public static final int MIN_ROW_COUNT = 10;
    public static final int MAX_ROW_COUNT = 256;

    public static final int MIN_COLUMN_COUNT = 10;
    public static final int MAX_COLUMN_COUNT = 256;

    public static final int DEFAULT_WIDTH = 1200;
    public static final int DEFAULT_HEIGHT = 800;

    public static final int DEFAULT_ROW_HEIGHT = 25;

    private LwsApplicationProperties() {

    }

}
