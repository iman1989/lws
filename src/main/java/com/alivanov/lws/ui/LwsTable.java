package com.alivanov.lws.ui;

import com.alivanov.lws.expr.DoubleValue;
import com.alivanov.lws.expr.ErrorValue;
import com.alivanov.lws.model.Cell;
import com.alivanov.lws.model.LwsTableModel;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.EventObject;

import static com.alivanov.lws.ui.LwsApplicationProperties.*;

class LwsTable extends JTable {

    LwsTable(LwsTableModel tableModel) {
        super(tableModel);

        initTable();

        setDefaultRenderer(Cell.class, new LwsCellRenderer());
        setDefaultRenderer(DoubleValue.class, new LwsDoubleRenderer());
        setDefaultRenderer(ErrorValue.class, new LwsErrorRenderer());

        setDefaultEditor(Cell.class, constructLwsEditor());
    }

    private void initTable() {
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setFillsViewportHeight(false);

        setRowSelectionAllowed(false);
        setColumnSelectionAllowed(false);
        setCellSelectionEnabled(false);

        getTableHeader().setReorderingAllowed(false);

        setPreferredScrollableViewportSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
        setRowHeight(DEFAULT_ROW_HEIGHT);

        setFont(LWS_CONTENT_FONT);
        setGridColor((Color) UIManager.getDefaults().get("TableHeader.background"));
        getTableHeader().setFont(LWS_HEADER_FONT);

        setSurrendersFocusOnKeystroke(true);
        putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);

        addKeyMappings();
    }

    private void addKeyMappings() {
        final InputMap inputMap = getInputMap(WHEN_FOCUSED);
        final ActionMap actionMap = getActionMap();

        registerAction("delete", KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), new DeleteAction(),
                inputMap, actionMap);
        registerAction("Copy", KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK, false), new CopyAction(),
                inputMap, actionMap);
        registerAction("Cut", KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK, false), new CutAction(),
                inputMap, actionMap);
        registerAction("Paste", KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_MASK, false), new PasteAction(),
                inputMap, actionMap);
    }

    private void registerAction(String actionDesc, KeyStroke keyStroke, Action action, InputMap inputMap, ActionMap actionMap) {
        inputMap.put(keyStroke, actionDesc);
        actionMap.put(actionDesc, action);
    }

    private class DeleteAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (getSelectedRow() != -1 && getSelectedColumn() != -1) {
                getModel().setValueAt(null, getSelectedRow(), getSelectedColumn());
            }
        }
    }

    private class CopyAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            Cell cell = (Cell) getValueAt(getSelectedRow(), getSelectedColumn());
            if (cell != null) {
                final Transferable content = new StringSelection(cell.getEditorView());
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(content, null);
            }
        }
    }

    private class CutAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            Cell cell = (Cell) getValueAt(getSelectedRow(), getSelectedColumn());
            if (cell != null) {
                final Transferable content = new StringSelection(cell.getEditorView());
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
                clipboard.setContents(content, null);
            }
            getModel().setValueAt(null, getSelectedRow(), getSelectedColumn());
        }
    }

    private class PasteAction extends AbstractAction {
        @Override
        public void actionPerformed(ActionEvent e) {
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            Transferable content = clipboard.getContents(this);

            if (content != null) {
                try {
                    String cellContent = (String) clipboard.getData(DataFlavor.stringFlavor);
                    getModel().setValueAt(cellContent, getSelectedRow(), getSelectedColumn());
                } catch (UnsupportedFlavorException | IOException e1) {
                }
            }
        }
    }

    @Override
    public LwsTableModel getModel() {
        return (LwsTableModel) super.getModel();
    }

    @Override
    public boolean editCellAt(int row, int column, EventObject e) {
        boolean result = super.editCellAt(row, column, e);

        final Component editor = getEditorComponent();
        if (editor == null) {
            return result;
        }

        if (e instanceof KeyEvent) {
            ((JTextComponent) editor).setText("");
        }

        return result;
    }

    public void initFocus() {
        requestFocus();
        changeSelection(0, 0, false, false);
    }

    // Default cell renderer for the table forwards to render particular classes of cell's values
    private static class LwsCellRenderer extends DefaultTableCellRenderer {
        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Cell cell = (Cell) value;

            if (cell != null) {
                return table.getDefaultRenderer(cell.getValue().getClass()).getTableCellRendererComponent(
                        table, cell.getValue(), isSelected, hasFocus, row, column);
            } else {
                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            }
        }
    }

    private static class LwsDoubleRenderer extends DefaultTableCellRenderer {
        LwsDoubleRenderer() {
            super();
            setHorizontalAlignment(JLabel.RIGHT);
        }
    }

    private static class LwsErrorRenderer extends DefaultTableCellRenderer {
        LwsErrorRenderer() {
            super();
            setHorizontalAlignment(JLabel.RIGHT);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            c.setForeground(Color.RED);
            c.setFont(c.getFont().deriveFont(Font.ITALIC));

            return c;
        }
    }

    private TableCellEditor constructLwsEditor() {
        final JTextField textField = new JTextField();
        textField.setFont(LwsApplicationProperties.LWS_CONTENT_FONT);
        // Change formula bar contents only if formula bar is not focused to avoid infinite loop from both document listeners
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (!LwsApplication.getInstance().getFormulaEditorField().hasFocus()) {
                    LwsApplication.getInstance().getFormulaEditorField().setText(textField.getText());
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                if (!LwsApplication.getInstance().getFormulaEditorField().hasFocus()) {
                    LwsApplication.getInstance().getFormulaEditorField().setText(textField.getText());
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        return new LwsCellEditor(textField);
    }

    private static class LwsCellEditor extends DefaultCellEditor {
        LwsCellEditor(JTextField textField) {
            super(textField);
        }

        @Override
        public Component getTableCellEditorComponent(JTable table, Object value,
                                                     boolean isSelected,
                                                     int row, int column) {
            Cell cell = (Cell) value;
            String text = ((cell == null) ? "" : cell.getEditorView());

            return super.getTableCellEditorComponent(table, text, isSelected, row, column);
        }
    }
}
