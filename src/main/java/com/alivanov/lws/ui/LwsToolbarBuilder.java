package com.alivanov.lws.ui;

import javax.swing.*;
import java.awt.*;

import static com.alivanov.lws.ui.LwsApplicationProperties.LWS_HEADER_FONT;

class LwsToolbarBuilder {

    static JToolBar constructToolBar(LwsApplication application) {
        final JToolBar toolbar = new JToolBar();

        final JPanel toolBarPanel = new JPanel();
        toolBarPanel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        toolBarPanel.setLayout(new GridBagLayout());

        final GridBagConstraints c = new GridBagConstraints();

        final JTextField cellField = new JTextField();
        cellField.setPreferredSize(new Dimension(60, 25));
        cellField.setHorizontalAlignment(SwingConstants.CENTER);
        cellField.setFont(LWS_HEADER_FONT);
        cellField.setBorder(BorderFactory.createCompoundBorder(cellField.getBorder(), BorderFactory.createEmptyBorder(0, 5, 0, 5)));

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0;
        c.gridx = 0;
        c.gridy = 0;
        toolBarPanel.add(cellField, c);

        final JLabel barCaption = new JLabel("f(x)", SwingConstants.CENTER);
        barCaption.setFont(LWS_HEADER_FONT.deriveFont(Font.ITALIC));
        barCaption.setBorder(BorderFactory.createCompoundBorder(barCaption.getBorder(), BorderFactory.createEmptyBorder(0, 15, 0, 5)));

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0;
        c.gridx = 1;
        c.gridy = 0;
        toolBarPanel.add(barCaption, c);

        final JTextField formulaField = new JTextField();
        formulaField.setPreferredSize(new Dimension(550, 25));
        formulaField.setFont(LWS_HEADER_FONT);
        formulaField.setBorder(BorderFactory.createCompoundBorder(formulaField.getBorder(), BorderFactory.createEmptyBorder(0, 5, 0, 5)));

        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1;
        c.gridx = 2;
        c.gridy = 0;
        toolBarPanel.add(formulaField, c);

        toolbar.add(toolBarPanel);
        toolbar.setFloatable(false);

        application.setCellField(cellField);
        application.setFormulaEditorField(formulaField);

        return toolbar;
    }

}
