package com.alivanov.lws;

import com.alivanov.lws.ui.LwsApplication;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> LwsApplication.getInstance().initAndRenderUI());
    }

}
