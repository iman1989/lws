package com.alivanov.lws.expr;

import java.util.Objects;

public class ErrorValue extends Value {

    public enum ErrorCode {
        INVALID_FORMULA("#FORMULA#"),
        MISSING_CELL("#CELL#"),
        COMPUTATION_ERROR("#ERROR#");

        private final String errorCode;

        ErrorCode(final String errorCode) {
            this.errorCode = errorCode;
        }

        @Override
        public String toString() {
            return errorCode;
        }
    }

    private final ErrorCode code;
    private final String cause;

    public ErrorValue(ErrorCode code, String cause) {
        this.code = code;
        this.cause = cause;
    }

    @Override
    public double toDoubleValue() {
        throw new ClassCastException("Invalid type conversion");
    }

    public String getCause() {
        return cause;
    }

    @Override
    public String toString() {
        return code.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ErrorValue that = (ErrorValue) o;
        return code == that.code &&
                Objects.equals(cause, that.cause);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, cause);
    }
}
