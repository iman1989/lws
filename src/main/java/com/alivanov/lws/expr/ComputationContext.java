package com.alivanov.lws.expr;

import com.alivanov.lws.model.CellGrid;
import com.alivanov.lws.model.Pos;

import java.util.HashSet;
import java.util.Set;

public final class ComputationContext {
    private final CellGrid grid;
    private final Set<Pos> visitedCells;

    private ComputationContext(CellGrid grid) {
        this.grid = grid;
        this.visitedCells = new HashSet<>();
    }

    private ComputationContext(ComputationContext copy) {
        this.grid = copy.grid;
        this.visitedCells = new HashSet<>(copy.getVisitedCells());
    }

    public static ComputationContext createNew(CellGrid grid) {
        return new ComputationContext(grid);
    }

    public ComputationContext copy() {
        return new ComputationContext(this);
    }

    public CellGrid getGrid() {
        return grid;
    }

    public Set<Pos> getVisitedCells() {
        return visitedCells;
    }
}
