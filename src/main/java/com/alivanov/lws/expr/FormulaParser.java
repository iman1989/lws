package com.alivanov.lws.expr;

import com.alivanov.lws.model.Pos;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FormulaParser {

    public Formula parseFormula(String formulaText) {
        final LwsParser parser = constructParser(formulaText);
        final CollectingVariablesListener collectingVariablesListener = new CollectingVariablesListener();
        parser.addParseListener(collectingVariablesListener);

        try {
            final ParseTree parseTree = parser.expression();
            final Map<String, Pos> variables = convertToPositions(collectingVariablesListener.getVariableSheetNames());
            return Formula.createFormula(formulaText, parseTree, variables);
        } catch (Exception e) {
            return Formula.createInvalidFormula(formulaText);
        }
    }

    private Map<String, Pos> convertToPositions(Set<String> variableSheetNames) {
        return variableSheetNames.stream().collect(Collectors.toMap(Function.identity(), Pos::from));
    }

    private LwsParser constructParser(String formulaText) {
        final CodePointCharStream codePointCharStream = CharStreams.fromString(formulaText);

        final LwsLexer lexer = new LwsLexer(codePointCharStream);
        lexer.removeErrorListeners();
        lexer.addErrorListener(new ThrowingErrorListener());

        final CommonTokenStream tokens = new CommonTokenStream(lexer);

        final LwsParser parser = new LwsParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new ThrowingErrorListener());

        return parser;
    }

    /*
        This error listener is needed for ANTLR parser to throw exceptions on invalid formulas instead of recovering
     */
    private static class ThrowingErrorListener extends BaseErrorListener {
        @Override
        public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
                throws ParseCancellationException {
            throw new ParseCancellationException("line " + line + ":" + charPositionInLine + " " + msg);
        }
    }

}
