package com.alivanov.lws.expr;

import com.alivanov.lws.model.Pos;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public final class Formula implements Serializable {

    private String formulaText;

    private transient ParseTree parseTree;
    private transient Map<String, Pos> variables;
    private transient boolean isValid;

    public static Formula createFormula(String formulaText, ParseTree parseTree, Map<String, Pos> variables) {
        Formula formula = new Formula();

        formula.formulaText = formulaText;
        formula.parseTree = parseTree;
        formula.variables = variables;
        formula.isValid = true;

        return formula;
    }

    public static Formula createInvalidFormula(String formulaText) {
        Formula formula = new Formula();

        formula.formulaText = formulaText;
        formula.variables = Collections.emptyMap();

        formula.isValid = false;

        return formula;
    }

    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.writeObject(formulaText);
    }

    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        formulaText = (String) stream.readObject();

        Formula temp = new FormulaParser().parseFormula(formulaText);
        parseTree = temp.parseTree;
        variables = temp.variables;
        isValid = temp.isValid;
    }

    public String getFormulaText() {
        return formulaText;
    }

    public ParseTree getParseTree() {
        return parseTree;
    }

    public Map<String, Pos> getVariables() {
        return variables;
    }

    public boolean isValid() {
        return isValid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Formula formula = (Formula) o;
        return Objects.equals(formulaText, formula.formulaText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(formulaText);
    }

    @Override
    public String toString() {
        return "\"" + formulaText + "\"";
    }
}
