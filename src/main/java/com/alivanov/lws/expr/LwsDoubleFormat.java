package com.alivanov.lws.expr;

import java.text.*;
import java.util.Locale;

/*
    Using single static format is NOT thread-safe.
    Used in this application only due to single-threaded calculations
    Should be rewritten to thread-local in multi-threaded calculations
 */
public class LwsDoubleFormat {

    public static final int MAXIMUM_DIGITS = 9;

    private static final NumberFormat DOUBLE_FORMAT = constructFormat();

    private static NumberFormat constructFormat() {
        NumberFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(MAXIMUM_DIGITS);
        return df;
    }

    public static String format(double val) {
        return DOUBLE_FORMAT.format(val);
    }

    // Strict parsing of the input, without omitting the trail string
    public static double parse(String str) throws ParseException {
        ParsePosition parsePosition = new ParsePosition(0);
        Number result = DOUBLE_FORMAT.parse(str, parsePosition);
        // Check if parse was successful
        if (parsePosition.getIndex() != str.length()) {
            throw new ParseException("Failed to parse string", parsePosition.getIndex());
        }
        return result.doubleValue();
    }

    private LwsDoubleFormat() {
    }

}
