package com.alivanov.lws.expr;

import java.util.Objects;

public class DoubleValue extends Value {

    private final double val;

    public DoubleValue(double val) {
        this.val = val;
    }

    public double toDoubleValue() {
        return this.val;
    }

    @Override
    public String toString() {
        return LwsDoubleFormat.format(val);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoubleValue that = (DoubleValue) o;
        return Double.compare(that.val, val) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(val);
    }
}
