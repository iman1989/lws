package com.alivanov.lws.expr;

import java.util.Objects;

public class StringValue extends Value {

    private final String val;

    public StringValue(String val) {
        this.val = val;
    }

    @Override
    public double toDoubleValue() {
        throw new ClassCastException("Invalid type conversion");
    }

    @Override
    public String toString() {
        return val;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringValue that = (StringValue) o;
        return Objects.equals(val, that.val);
    }

    @Override
    public int hashCode() {
        return Objects.hash(val);
    }
}
