package com.alivanov.lws.expr;

import java.io.Serializable;

public abstract class Value implements Serializable {

    public abstract double toDoubleValue();

    @Override
    public abstract String toString();
}
