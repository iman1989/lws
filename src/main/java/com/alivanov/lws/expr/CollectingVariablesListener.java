package com.alivanov.lws.expr;

import java.util.HashSet;
import java.util.Set;

class CollectingVariablesListener extends LwsBaseListener {

    private final Set<String> variableSheetNames;

    CollectingVariablesListener() {
        this.variableSheetNames = new HashSet<>();
    }

    public Set<String> getVariableSheetNames() {
        return variableSheetNames;
    }

    @Override
    public void exitVar(LwsParser.VarContext ctx) {
        variableSheetNames.add(ctx.VARIABLE().toString());
    }
}
