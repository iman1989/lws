package com.alivanov.lws.expr;

import com.alivanov.lws.model.Cell;
import com.alivanov.lws.model.Pos;

import java.text.ParseException;
import java.util.Map;

public class ComputeExpressionVisitor extends LwsBaseVisitor<Value> {

    private final ComputationContext context;
    private final Map<String, Pos> variables;

    public ComputeExpressionVisitor(ComputationContext context, Map<String, Pos> variables) {
        this.context = context;
        this.variables = variables;
    }

    @Override
    public Value visitMultDiv(LwsParser.MultDivContext ctx) {
        final Value leftValue = visit(ctx.expression(0));
        if (leftValue instanceof ErrorValue) {
            return leftValue;
        }

        final Value rightValue = visit(ctx.expression(1));
        if (rightValue instanceof ErrorValue) {
            return rightValue;
        }

        try {
            if (ctx.op.getType() == LwsParser.TIMES) {
                return new DoubleValue(leftValue.toDoubleValue() * rightValue.toDoubleValue());
            } else {
                double rightDouble = rightValue.toDoubleValue();
                if (rightDouble == 0.0) {
                    throw new ArithmeticException("Division by zero");
                }
                return new DoubleValue(leftValue.toDoubleValue() / rightDouble);
            }
        } catch (ClassCastException | ArithmeticException e) {
            return new ErrorValue(ErrorValue.ErrorCode.COMPUTATION_ERROR, e.getMessage());
        }
    }

    @Override
    public Value visitPlusMinus(LwsParser.PlusMinusContext ctx) {
        final Value leftValue = visit(ctx.expression(0));
        if (leftValue instanceof ErrorValue) {
            return leftValue;
        }

        final Value rightValue = visit(ctx.expression(1));
        if (rightValue instanceof ErrorValue) {
            return rightValue;
        }

        try {
            if (ctx.op.getType() == LwsParser.PLUS) {
                return new DoubleValue(leftValue.toDoubleValue() + rightValue.toDoubleValue());
            } else {
                return new DoubleValue(leftValue.toDoubleValue() - rightValue.toDoubleValue());
            }
        } catch (ClassCastException e) {
            return new ErrorValue(ErrorValue.ErrorCode.COMPUTATION_ERROR, e.getMessage());
        }
    }

    @Override
    public Value visitTerm(LwsParser.TermContext ctx) {
        return visit(ctx.power());
    }

    @Override
    public Value visitPow(final LwsParser.PowContext ctx) {
        final Value baseValue = visit(ctx.atom());

        if (ctx.POW() != null) {
            if (baseValue instanceof ErrorValue) {
                return baseValue;
            }

            final Value exponentValue = visit(ctx.power());
            if (exponentValue instanceof ErrorValue) {
                return exponentValue;
            }

            try {
                return new DoubleValue(Math.pow(baseValue.toDoubleValue(), exponentValue.toDoubleValue()));
            } catch (ClassCastException e) {
                return new ErrorValue(ErrorValue.ErrorCode.COMPUTATION_ERROR, e.getMessage());
            }
        } else {
            return baseValue;
        }
    }

    @Override
    public Value visitNegation(LwsParser.NegationContext ctx) {
        final Value exprValue = visit(ctx.atom());
        if (exprValue instanceof ErrorValue) {
            return exprValue;
        }

        try {
            return new DoubleValue(-1 * exprValue.toDoubleValue());
        } catch (ClassCastException e) {
            return new ErrorValue(ErrorValue.ErrorCode.COMPUTATION_ERROR, e.getMessage());
        }
    }

    @Override
    public Value visitParens(LwsParser.ParensContext ctx) {
        return visit(ctx.expression());
    }

    @Override
    public Value visitNum(LwsParser.NumContext ctx) {
        try {
            return new DoubleValue(LwsDoubleFormat.parse(ctx.NUMBER().getText()));
        } catch (ParseException e) {
            return new ErrorValue(ErrorValue.ErrorCode.INVALID_FORMULA, e.getMessage());
        }
    }

    @Override
    public Value visitVar(LwsParser.VarContext ctx) {
        final Cell cell = context.getGrid().get(variables.get(ctx.VARIABLE().toString()));
        if (cell != null) {
            return cell.compute(context.copy());
        } else {
            return new ErrorValue(ErrorValue.ErrorCode.MISSING_CELL,
                    "No data found in cell " + ctx.VARIABLE().toString());
        }

    }
}
