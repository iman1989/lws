grammar Lws;

expression
   : expression op=(TIMES | DIV) expression     # MultDiv
   | expression op=(PLUS | MINUS) expression    # PlusMinus
   | power                                      # Term
   ;

power
    : atom (POW power)?                         # Pow
    ;

atom
   : MINUS atom                                 # Negation
   | NUMBER                                     # Num
   | VARIABLE                                   # Var
   | LPAREN expression RPAREN                   # Parens
   ;

VARIABLE
   : VALID_ID_COLUMN+ VALID_ID_ROW+
   ;

fragment VALID_ID_COLUMN
   : ('a' .. 'z') | ('A' .. 'Z')
   ;

fragment VALID_ID_ROW
   : ('0' .. '9')
   ;

NUMBER
    : ('0' .. '9')+ ('.' ('0' .. '9')+)?
    | '.' ('0' .. '9')+
    ;

LPAREN
   : '('
   ;

RPAREN
   : ')'
   ;

PLUS
   : '+'
   ;

MINUS
   : '-'
   ;

TIMES
   : '*'
   ;

DIV
   : '/'
   ;

POINT
   : '.'
   ;

POW
   : '^'
   ;

WS
   : [ \r\n\t] + -> skip
   ;