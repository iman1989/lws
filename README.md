LWS
===========================
Lightweight spreadsheet implemented in plain Java and Swing

## Warning
This program is written and tested on Windows machine. Same behaviour and look&fell are not guaranteed on Mac OS.
## How to run
To run the spreadsheet, please navigate to lws.jar and execute the following:
~~~~
java -jar lws.jar
~~~~
## How to build
To build LWS from the source code, please execute:
~~~~
mvn clean install
~~~~
This will compile code, run tests and assemble lws.jar in the target/ folder

## How to use
- Start editing by typing into a cell directly to erase its previous contents
- Or press F2 to edit current cell's contents
- Press delete to erase cell's contents
- Press enter or change focus to finalize cell editing
- Use navigation and formula bar for even easier navigation and input
- Use right-click menu or standart hotkeys to copy, cut and paste cells
- Use main menu to save, load or create new worksheets
- Have fun!

## Features
- Input of string and numeric values. Cell type is determined automatically based on input. Float values are using **dots** as separators.
- Input of formula values. Formulas start with = symbol. Formulas support cell references, operations + - / * ^ and parentheses.
- Automatic recalculation of formula values when a change on the spreadsheet occurs.
- Automatic detection of missing cells, invalid formulas, type casting erros and circular references.
- Saving and loading worksheets to disk.
- Copy, cut and paste features.